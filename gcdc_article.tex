% !TEX TS-program = pdflatex
% !TEX encoding = UTF-8 Unicode

% This is a simple template for a LaTeX document using the "article" class.
% See "book", "report", "letter" for other types of document.

\documentclass[11pt]{article} % use larger type; default would be 10pt

\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)

%%% Examples of Article customizations
% These packages are optional, depending whether you want the features they provide.
% See the LaTeX Companion or other references for full information.

%%% PAGE DIMENSIONS
\usepackage{geometry} % to change the page dimensions
\geometry{a4paper} % or letterpaper (US) or a5paper or....
% \geometry{margin=2in} % for example, change the margins to 2 inches all round
% \geometry{landscape} % set up the page for landscape
%   read geometry.pdf for detailed page layout information

\usepackage{graphicx} % support the \includegraphics command and options
\setlength\parindent{0pt}

\usepackage[parfill]{parskip} % Activate to begin paragraphs with an empty line rather than an indent

%%% PACKAGES
\usepackage{booktabs} % for much better looking tables
\usepackage{array} % for better arrays (eg matrices) in maths
\usepackage{paralist} % very flexible & customisable lists (eg. enumerate/itemize, etc.)
\usepackage{verbatim} % adds environment for commenting out blocks of text & for better verbatim
%\usepackage{subfig} % make it possible to include more than one captioned figure/table in a single float
% These packages are all incorporated in the memoir class to one degree or another...
\usepackage{amsmath}
\usepackage{amsfonts}
%\numberwithin{equation}{section}
\usepackage[font=small,labelfont=bf,labelsep=period]{caption}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{listings}
\usepackage{color}
%\usepackage[numbered]{mcode}
\usepackage{subcaption}
\usepackage{float}
\usepackage{chngcntr}
\counterwithin{figure}{section}
%\usepackage[T1]{fontenc}
\usepackage{url}
\usepackage[hidelinks]{hyperref}
\usepackage{placeins}


%%% HEADERS & FOOTERS
\usepackage{fancyhdr} % This should be set AFTER setting up the page geometry
\pagestyle{fancy} % options: empty , plain , fancy
\renewcommand{\headrulewidth}{0pt} % customise the layout...
\lhead{}\chead{}\rhead{}
\lfoot{}\cfoot{\thepage}\rfoot{}

%%% SECTION TITLE APPEARANCE
\usepackage{sectsty}
\allsectionsfont{\sffamily\mdseries\upshape} % (See the fntguide.pdf for font help)
% (This matches ConTeXt defaults)

%%% ToC (table of contents) APPEARANCE
\usepackage[nottoc,notlof,notlot]{tocbibind} % Put the bibliography in the ToC
\usepackage[titles,subfigure]{tocloft} % Alter the style of the Table of Contents
\renewcommand{\cftsecfont}{\rmfamily\mdseries\upshape}
\renewcommand{\cftsecpagefont}{\rmfamily\mdseries\upshape} % No bold!


%\captionsetup[table]{name=Tabell} % Enable for swedish table title
%\captionsetup{margin=10pt,font=small,labelfont=bf}
%\captionsetup[figure]{name=Figur} % Enable for swedish figure title
\captionsetup[subfigure]{margin=6pt,font=small,labelfont=bf}

%%% END Article customizations

%%% The "real" document content comes below...

\title{GCDC article - vehicle detection}

\author{Björn Persson Mattsson}

%\date{} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed 
         
         
%\addbibresource{references} % New command, use if available
%\bibliography{references} % Legacy command

\begin{document}
\maketitle


\section{Method}

For the vehicle detection, a \textit{convolutional neural network} was used, with two different classes as output: vehicles and non-vehicles.
When using a CNN, the network architecture plays a great role in the function of the network.
By using two slightly different architectures depending on whether the network was used for training or for detection, it is possible to achieve localization at the same time as detection, a property which is very handy.
The two networks are essentially identical, except for the final layer.
The identical parts of the CNN is shown in Figure \ref{fig:gcdc_cnn}.
In order to not lose information during the propagation through the network, zero-padding was used for the convolutional layers.
For the implementation, the open-source library \textit{tiny-cnn} \cite{tinycnn} was used.
The image pre- and post-processing was performed using OpenCV.

\begin{figure}[bht]
\centering
  \includegraphics[width=1.0\linewidth]{fig/gcdc_cnn}
\caption{Sketch of the convolutional neural network that was used. The CNN takes the three color channels of an image as input, and then passes the data through a series of convolutional and max-pooling layers. When the data has been propagated through the network, the two output spatial maps have been reduced to a fourth of the input image's size. The two output features correspond to the \textit{vehicle} class and the \textit{non-vehicle} class of the dataset used for training.}
\label{fig:gcdc_cnn}
\end{figure}

During the training of the CNN, a network architecture with an additional final max-pooling layer, condensing the result to just one pixel for each feature, was used to achieve a normal classification.
Looking at the resulting output of this training network, the activation value for each output feature's single pixel was used as the soft-max likelihood that the input image belonged to the corresponding class.
Thus, a normal classification-based network training could be performed.
However, when using the network for detection and localization, this final max-pooling layer is not used. 
Instead the resulting output becomes a reduced spatial map, where the activation of a pixel corresponds to the resemblance of a specific class, and the architecture looks as in Figure \ref{fig:gcdc_cnn}.
For example, a car in an image would be lit up by brighter pixels than the road it drives on in the \textit{vehicle} feature output, while the road would be more lit up than the car in the \textit{non-vehicle} feature output.
An example of this is seen in Figure \ref{fig:example}.


\subsection{Training}

The training data were created from a mix of existing datasets, such as the \textit{KITTI Dataset} \cite{Geiger2013IJRR}, and data gathered by the authors. 
In total, 3608 images were used, of which 1973 were negatives (depicting non-vehicles) and 1635 were positives (depicting vehicles). 
From these images, 12 \% ($\approx 433$) were randomly picked to act as testing data, and were thus not used during the training.
See Figure \ref{fig:example_data} for some examples of the training data.


\begin{figure}[bht]
\centering
  \includegraphics[scale=1.0]{fig/example_data}
\caption{Six examples of training images. On the top row are three positive images depicting two cars and one truck. On the bottom row are three negative images depicting a sign near an intersection, a bridge on a highway, and a traffic barrier on a highway.}
\label{fig:example_data}
\end{figure}

Each image was resized to 32x32 pixels before being passed as input to the CNN.
All data were normalized to zero-mean and unit variance for each color channel, and the normalization constants were saved for later use during the detection phase.

\subsection{Detection}

When using the CNN for detection, three steps were involved. 
The first step was the preprocessing, in which the input image was resized to 200x112 pixels, normalized according to the constants found during the training phase, and converted to a data format compatible with the tiny-cnn library.
The next step was the actual detection and localization, that is, the CNN forward propagation.
In the final step, the resulting spatial map was post-processed in order to determine where in the image vehicles were present.
The middle step was the most time-consuming one.
An example of an input image and the corresponding CNN output is displayed in Figure \ref{fig:example}.

%\begin{figure}[bht]
%\centering
%\begin{subfigure}{.7\textwidth}
%  \centering
%  \includegraphics[width=0.7\linewidth]{fig/example_input}
%  \caption{Input image.}
%  \label{fig:example_input}
%\end{subfigure}
%\begin{subfigure}{.7\textwidth}
%  \centering
%  \includegraphics[width=0.7\linewidth]{fig/example_output}
%  \caption{Output of the CNN. The left image shows the result from the \textit{vehicle} feature while the right image shows the \textit{non-vehicle} feature. Bright pixels correspond to a high activation for the class at that approximate location in the input image.}
%  \label{fig:example_output}
%\end{subfigure}
%\caption{Example of an input image and the resulting reduced spatial map when using the CNN for detection.}
%\label{fig:example_output_total}
%\end{figure}

The goal of the post-processing was to extract location information about the detected vehicles from the reduced spatial maps shown in Figures \ref{fig:example_4} and \ref{fig:example_5}. 
This was done by first subtracting the non-vehicle spatial map from the vehicle spatial map, which resulted in the suppression of image areas where the CNN was unsure whether there was a vehicle or not (see Figure \ref{fig:example_3}).
The resulting image was smoothed using Gaussian blur to remove noise, before being thresholded (see Figure \ref{fig:example_2}).
Following this, a connected-component analysis was run on the thresholded image, resulting in a list of blobs which each corresponded to a likely vehicle.
Finally, a bounding box was found around each blob to represent each vehicle as a rectangle, as shown in \ref{fig:example_1}.
Based on these rectangles, vehicle properties such as position and size can be inferred and passed on to the rest of the system, as long as the relationship between the camera and the real world is known (as described in Section \ref{}).\todo{Reference to pixel-2-world mapping}


\begin{figure}[h!]
\centering
\begin{subfigure}[t]{.4\textwidth}
  \centering
  \includegraphics[height=3cm]{fig/example_4}
  \caption{Activation of the \textit{vehicle} class.}
  \label{fig:example_4}
\end{subfigure}%
\begin{subfigure}[t]{.4\textwidth}
  \centering
  \includegraphics[height=3cm]{fig/example_5}
  \caption{Activation of the \textit{non-vehicle} class.}
  \label{fig:example_5}
\end{subfigure}
\begin{subfigure}[t]{.4\textwidth}
  \centering
  \includegraphics[height=3cm]{fig/example_3}
  \caption{Difference between the vehicle and non-vehicle activations, with added Gaussian blur.}
  \label{fig:example_3}
\end{subfigure}%
\begin{subfigure}[t]{.4\textwidth}
  \centering
  \includegraphics[height=3cm]{fig/example_2}
  \caption{Thresholded version of Figure \ref{fig:example_3}.}
  \label{fig:example_2}
\end{subfigure}
\begin{subfigure}[t]{1\textwidth}
  \centering
  \includegraphics[height=6cm]{fig/example_1}
  \caption{Input image with bounding boxes of all detections shown as red rectangles.}
  \label{fig:example_1}
\end{subfigure}
\caption{A visualization of the processing of an image.}
\label{fig:example}
\end{figure}


\FloatBarrier
\section{Discussion}

As the intention is for the algorithm to run in real-time in a vehicle, it must not be too slow.
Ideally, the vehicle detection should execute at around 20 Hz, preferably even faster.
However, because the algorithm currently is not designed for high-performance computing via e.g. GPU acceleration, 20 Hz was not reached.
Instead, the execution speed was around 0.7 Hz when running the vehicle detection (as well as other processes) on the on-board computer, which was much too slow, and around 3 Hz when running the vehicle detection on a separate laptop, which was slow but still usable.
Using a CNN with a more complex network architecture, something which might be necessary to handle more complex situations, would mean that the execution time would be even longer.
It is therefore a necessity to plan for a version of the algorithm that runs on a GPU in order to speed up the execution time.

\subsection{Evaluation}

The algorithm worked generally well, in particular at distances from circa 10 to 100 meters.
Outside of that interval, various problems caused the results to be more unstable.
At too far distances, two problems dominated: (1) The resolution of the input image was too low and the vehicles could not be discerned, and (2) the connected-component analysis caused several objects to merge together to one big.
At very short distances, the CNN could identify parts of vehicles correctly, but failed to classify for example rear windows as vehicles.
This, in turn, caused the connected-component analysis to detect two different objects (e.g. a roof and a rear of a car) without understanding that the two parts belonged to the same vehicle.
This type of problem can be seen for the truck in Figure \ref{fig:example_1}.
The merging of several vehicles into a single seemingly large one can also occur when vehicles are too close to or behind each other from the camera's point of view.
Figure \ref{fig:example} shows this problem for the cluster of cars in front, when three vehicles are merged into one.

From time to time, the algorithm made incorrect detections by classifying non-vehicles as vehicles.
The CNN reacted strongly on sharp edges and patterns in the belief that they were vehicles, something which at least partly is a consequence of the used training data.
Some examples of such objects are road signs, fences, some types of road markings, and box-like items, as shown in Figure \ref{fig:fneg}.

\begin{figure}[bht]
\centering
\begin{subfigure}[t]{.4\textwidth}
  \centering
  \includegraphics[height=3cm]{fig/fneg_2}
  \caption{Road-side fences. The false detection is also merged with the correctly classified truck.}
  \label{fig:fneg_2}
\end{subfigure}%
\begin{subfigure}[t]{.4\textwidth}
  \centering
  \includegraphics[height=3cm]{fig/fneg_4}
  \caption{Road pattern with sharp edges.}
  \label{fig:fneg_4}
\end{subfigure}
\begin{subfigure}[t]{.4\textwidth}
  \centering
  \includegraphics[height=3cm]{fig/fneg_7}
  \caption{A tent.}
  \label{fig:fneg_7}
\end{subfigure}%
\begin{subfigure}[t]{.4\textwidth}
  \centering
  \includegraphics[height=3cm]{fig/fneg_8}
  \caption{A road sign.}
  \label{fig:fneg_8}
\end{subfigure}
\caption{Examples of false negatives detected by the CNN. Bounding boxes of detections are outlined in red.}
\label{fig:fneg}
\end{figure}

%[Evaluation of the algorithm. How well it worked. How many true positives were detected? How often, and on what, did it give false positives?]
%
%[--- Often reacted on signs and shadows. Training dataset needs to be improved and increased to "unlearn" rare non-vehicle objects that are classified as cars.]
%
%[--- Long and diagonal detections became very "large" detections due to the bounding box.]

\subsection{Future improvements}

In order to improve the algorithm in the future, the first step should be to make it faster via e.g. GPU acceleration.
Making the temporal cost of the CNN forward propagation smaller would not only make it possible to run the algorithm at a higher rate, it would also allow for a more complex network architecture.
With a larger network, the accuracy of the CNN can potentially be greatly improved with fewer incorrect detections.
Another possible improvement would be to train the CNN to not only recognize vehicles and non-vehicles, but take into account more classes, such as cars, motorbikes, and trucks.
Other objects, for example traffic signs, could possibly also be detected, although we would then leave the domain of only vehicle detection.
A quicker GPU-based algorithm would also allow for the usage of a larger input image, something which would give the vehicle-detection system a higher resolution and the potential to detect vehicles at a larger range.

Using some other technique for the post-processing part of the algorithm would also likely improve the results.
The current method of thresholding the CNN output and taking the bounding boxes of the connected components leads to problems when the vehicles in the input image are positioned too closely to one another.
The detections may then merge together, and a bounding box that surrounds both vehicles is found.
This, in turn, causes incorrect detections which may seem closer or larger than what they really are.
A possible way to get around this problem is to let the neural network also output the proposed bounding boxes of the vehicles. 
This has been done in \cite{fjslk} \todo{missing reference, will find it /B} to great effect, but requires more complex training data and may potentially take a large toll on the time efficiency of the algorithm.






%\clearpage
%\FloatBarrier
\bibliographystyle{ieeetr}
\bibliography{references}

%\nocite{*} % Cites everything from the bibliography
%\printbibliography % This command is new in biblatex. Do not attempt to use style files as with old latex.




%\begin{figure}[bht]
%\centering
%\begin{subfigure}{.7\textwidth}
%  \centering
%  \includegraphics[width=1.0\linewidth]{fig/q1c_avg_popsize_over_time}
%  \caption{Sub caption.}
%  \label{fig:q1c_avg_popsize_over_time_1}
%\end{subfigure}
%\begin{subfigure}{.7\textwidth}
%  \centering
%  \includegraphics[width=1.0\linewidth]{fig/q1c_avg_popsize_over_time_extra}
%  \caption{Sub caption.}
%  \label{fig:q1c_avg_popsize_over_time_extra}
%\end{subfigure}
%\caption{Caption.}
%\label{fig:q1c_avg_popsize_over_time}
%\end{figure}


%\begin{figure}[bht]
%\centering
%  \includegraphics[width=1.0\linewidth]{fig/q1c_avg_popsize_over_time_extra}
%\caption{Caption.}
%\label{fig:q1c_avg_popsize_over_time_extra}
%\end{figure}


%\clearpage
%\section*{Appendix}
%\subsection*{Matlab code}
%The Matlab code is attached in the \texttt{code.zip} file.

% \FloatBarrier
% \begin{thebibliography}{9}

% %\bibitem{wiki_odd_symmetric}
% %Wikipedia contributors. Even and odd functions [Internet]. Wikipedia, The Free Encyclopedia; 2014 Dec 2, 03:14 UTC [cited 2015 Jan 25]. Available from: \url{http://en.wikipedia.org/w/index.php?title=Even_and_odd_functions&oldid=636262968}.



% \end{thebibliography}


\end{document}
